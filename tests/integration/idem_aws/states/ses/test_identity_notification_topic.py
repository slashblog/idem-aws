import uuid
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-notification-topic-" + str(uuid.uuid4()),
    "notification_type": "Complaint",
    "enabled": False,
}


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.asyncio
@pytest.mark.dependency(name="present")
async def test_present(
    hub, ctx, aws_ses_domain_identity, aws_sns_topic, __test, cleanup
):
    global PARAMETER
    PARAMETER["identity"] = aws_ses_domain_identity.get("domain")
    PARAMETER["sns_topic"] = aws_sns_topic.get("resource_id")

    # Create Notification for a topic
    ctx["test"] = __test

    ret = await hub.states.aws.ses.identity_notification_topic.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    if __test:
        assert (
            f"Would create aws.ses.identity_notification_topic '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Created aws.ses.identity_notification_topic '{PARAMETER['name']}'"
            in ret["comment"]
        )
        PARAMETER["resource_id"] = resource["resource_id"]

    assert PARAMETER["sns_topic"] == resource.get("sns_topic")
    assert PARAMETER["identity"] == resource.get("identity")
    assert PARAMETER["notification_type"] == resource.get("notification_type")
    assert PARAMETER["enabled"] == resource.get("enabled")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    # Describe Notification topic
    global PARAMETER
    describe_ret = await hub.states.aws.ses.identity_notification_topic.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that describe output format is correct
    assert "aws.ses.identity_notification_topic.present" in describe_ret.get(
        resource_id
    )
    described_resource = describe_ret.get(resource_id).get(
        "aws.ses.identity_notification_topic.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["sns_topic"] == described_resource_map.get("sns_topic")
    assert PARAMETER["identity"] == described_resource_map.get("identity")
    assert PARAMETER["notification_type"] == described_resource_map.get(
        "notification_type"
    )
    assert PARAMETER["enabled"] == described_resource_map.get("enabled")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update_enable(hub, ctx, __test):
    # Updating Enable

    PARAMETER["enabled"] = True
    ctx["test"] = __test

    ret = await hub.states.aws.ses.identity_notification_topic.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("enabled") is True
    if __test:
        assert (
            f"Would update aws.ses.identity_notification_topic '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Updated aws.ses.identity_notification_topic '{PARAMETER['name']}'"
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update_sns_topic(hub, ctx, aws_sns_topic_2, __test):
    # Updating Notification topic

    PARAMETER["sns_topic"] = aws_sns_topic_2.get("resource_id")
    ctx["test"] = __test

    ret = await hub.states.aws.ses.identity_notification_topic.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert PARAMETER["sns_topic"] == resource.get("sns_topic")
    if __test:
        assert (
            f"Would update aws.ses.identity_notification_topic '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Updated aws.ses.identity_notification_topic '{PARAMETER['name']}'"
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["present"])
async def test_absent(hub, ctx, __test):
    # Clears the SnsTopic

    ctx["test"] = __test
    ret = await hub.states.aws.ses.identity_notification_topic.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    if __test:
        assert (
            f"Would delete aws.ses.identity_notification_topic '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Deleted aws.ses.identity_notification_topic '{PARAMETER['name']}'"
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.ses.identity_notification_topic.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.ses.identity_notification_topic '{PARAMETER['name']}' already absent"
        in ret["comment"]
    )
    assert (not ret.get("old_state")) and (not ret.get("new_state"))


@pytest.mark.asyncio
async def test_domain_id_absent_with_none_resource_id(hub, ctx):
    domain_id_temp_name = "idem-test-domain_id-" + str(uuid.uuid4())
    # Deletes identity notification topic with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ses.identity_notification_topic.absent(
        ctx, name=domain_id_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.ses.identity_notification_topic '{domain_id_temp_name}' already absent"
        in ret["comment"]
    )


@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ses.identity_notification_topic.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
