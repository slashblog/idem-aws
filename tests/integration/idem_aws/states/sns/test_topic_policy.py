import copy
import json
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_topic_policy(hub, ctx, aws_sns_topic):
    topic_policy_name = "idem-test-topic-policy" + str(uuid.uuid4())

    topic_arn = aws_sns_topic.get("resource_id")
    default_policy = hub.tool.aws.sns.sns_utils.get_default_topic_policy(topic_arn)
    policy_dict = {
        "Version": "2012-10-17",
        "Id": "id-1",
        "Statement": [
            {
                "Sid": "__default_statement_ID",
                "Effect": "Allow",
                "Principal": {"AWS": "*"},
                "Action": [
                    "SNS:GetTopicAttributes",
                    "SNS:SetTopicAttributes",
                    "SNS:AddPermission",
                    "SNS:RemovePermission",
                    "SNS:DeleteTopic",
                    "SNS:Subscribe",
                    "SNS:ListSubscriptionsByTopic",
                    "SNS:Publish",
                ],
                "Resource": "topic_arn",
                "Condition": {"StringEquals": {"AWS:SourceOwner": "acct_id"}},
            }
        ],
    }
    policy_dict["Statement"][0]["Resource"] = topic_arn
    policy_dict["Statement"][0]["Condition"]["StringEquals"][
        "AWS:SourceOwner"
    ] = topic_arn.split(":")[4]
    policy = json.dumps(policy_dict, separators=(", ", ": "))
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Update topic_policy with test flag
    ret = await hub.states.aws.sns.topic_policy.present(
        test_ctx, name=topic_policy_name, topic_arn=topic_arn, policy=policy
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would update policy attribute of aws.sns.topic '{topic_arn}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert topic_policy_name == resource.get("name")
    assert topic_arn == resource.get("topic_arn")
    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        policy, resource.get("policy")
    )
    assert resource.get("resource_id")

    # Update topic_policy
    ret = await hub.states.aws.sns.topic_policy.present(
        ctx, name=topic_policy_name, topic_arn=topic_arn, policy=policy
    )
    assert ret["result"], ret["comment"]
    assert f"Updated policy attribute of aws.sns.topic '{topic_arn}'" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert topic_policy_name == resource.get("name")
    assert topic_arn == resource.get("topic_arn")
    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        policy, resource.get("policy")
    )
    resource_id = resource.get("resource_id")

    # With no changes
    ret = await hub.states.aws.sns.topic_policy.present(
        ctx, name=topic_policy_name, topic_arn=topic_arn, policy=policy
    )
    assert ret["result"], ret["comment"]
    assert (
        f"No changes required for aws.sns.topic_policy '{topic_policy_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    assert ret.get("old_state") == ret.get("new_state")
    resource = ret.get("new_state")
    assert topic_policy_name == resource.get("name")
    assert topic_arn == resource.get("topic_arn")
    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        policy, resource.get("policy")
    )

    # Describe topic policy
    describe_ret = await hub.states.aws.sns.topic_policy.describe(ctx)
    assert resource_id in describe_ret
    # Verify that describe output format is correct
    assert "aws.sns.topic_policy.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.sns.topic_policy.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert topic_arn == described_resource_map.get("topic_arn")
    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        policy, described_resource_map.get("policy")
    )

    # Delete topic policy with test flag
    ret = await hub.states.aws.sns.topic_policy.absent(
        test_ctx, name=topic_policy_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete aws.sns.topic_policy '{topic_policy_name}'" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        default_policy, resource.get("policy")
    )

    # Delete topic policy
    ret = await hub.states.aws.sns.topic_policy.absent(
        ctx, name=topic_policy_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Deleted aws.sns.topic_policy for aws.sns.topic '{topic_arn}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        default_policy, resource.get("policy")
    )
