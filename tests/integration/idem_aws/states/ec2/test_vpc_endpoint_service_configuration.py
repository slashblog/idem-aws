"""Tests for validating EC2 VPC Endpoint Service Configurations."""
import uuid

import pytest


@pytest.fixture(scope="module")
def shared_data():
    parameter = {"name": "idem-test-resource-" + str(uuid.uuid4())}
    return parameter


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_present(
    hub, ctx, __test, shared_data, aws_vpc_endpoint_service_load_balancer_1
):
    # Create the resource
    ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.present(
        ctx,
        name=shared_data["name"],
        acceptance_required=False,
        gateway_load_balancer_arns=[
            aws_vpc_endpoint_service_load_balancer_1.get("resource_id")
        ],
        supported_ip_address_types=["ipv4"],
        tags=[{"Key": "test", "Value": "test_value"}],
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    assert "tags" in resource

    if __test == 0:
        assert (
            f"Would create aws.ec2.vpc_endpoint_service_configuration '{shared_data['name']}'"
            in ret["comment"]
        )
        return
    elif __test == 1:
        assert (
            f"Created aws.ec2.vpc_endpoint_service_configuration '{shared_data['name']}'"
            in ret["comment"]
        )
        assert "test" in resource["tags"]

    shared_data["resource_id"] = resource["resource_id"]
    assert not ret["old_state"], ret["comment"]
    assert ret["new_state"], ret["comment"]
    assert shared_data["name"] == resource.get("name")


@pytest.mark.dependency(depends=["present"])
async def test_get(hub, ctx, shared_data, aws_vpc_endpoint_service_load_balancer_1):
    # Now get the resource with exec.get
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=shared_data["name"],
        resource_id=shared_data["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
    assert gateway_load_balancer_arns
    assert gateway_load_balancer_arns[
        0
    ] == aws_vpc_endpoint_service_load_balancer_1.get("resource_id")


@pytest.mark.dependency(depends=["present"])
async def test_update(hub, ctx, shared_data, aws_vpc_endpoint_service_load_balancer_2):
    # Now Update the resource
    ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.present(
        ctx,
        name=shared_data["name"],
        resource_id=shared_data["resource_id"],
        acceptance_required=True,
        gateway_load_balancer_arns=[
            aws_vpc_endpoint_service_load_balancer_2["resource_id"]
        ],
        supported_ip_address_types=["ipv4"],
        tags=[{"Key": "update_test", "Value": "update_test_value"}],
    )

    assert (
        f"Updated aws.ec2.vpc_endpoint_service_configuration '{shared_data['name']}'"
        in ret["comment"]
    )
    assert ret["result"], ret["comment"]

    assert ret.get("old_state") and ret.get("new_state")
    resource = ret["new_state"]
    assert resource
    gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
    assert gateway_load_balancer_arns
    assert gateway_load_balancer_arns[
        0
    ] == aws_vpc_endpoint_service_load_balancer_2.get("resource_id")
    assert "tags" in resource
    # The key "test" is marked for deletion as it is not passed into present again
    assert "test" not in resource["tags"]
    # the new tag should be added
    assert "update_test" in resource["tags"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(depends=["present"])
async def test_describe(hub, ctx, shared_data):
    assert shared_data.get(
        "resource_id", None
    ), "The resource might not have been created"
    ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.describe(ctx)
    resource_id = shared_data["resource_id"]
    assert resource_id in ret
    assert "aws.ec2.vpc_endpoint_service_configuration.present" in ret[resource_id]
    described_resource = ret[resource_id].get(
        "aws.ec2.vpc_endpoint_service_configuration.present"
    )
    assert described_resource


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_absent(hub, ctx, __test, shared_data):
    assert shared_data.get(
        "resource_id", None
    ), "The resource might not have been created"

    # Delete the resource
    ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.absent(
        ctx,
        name=shared_data["name"],
        resource_id=shared_data["resource_id"],
    )

    if __test == 0:
        assert (
            f"Would delete aws.ec2.vpc_endpoint_service_configuration '{shared_data['name']}'"
            in ret["comment"]
        )
        return
    elif __test == 1:
        assert (
            f"Deleted aws.ec2.vpc_endpoint_service_configuration '{shared_data['name']}'"
            in ret["comment"]
        )

        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")
        ret.get("old_state")

        # Now get the resource with exec - Should not exist
        ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
            ctx,
            name=shared_data["name"],
            resource_id=shared_data["resource_id"],
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"] is None
        assert "result is empty" in str(ret["comment"])

    if __test == 0:
        return

    # Try deleting the resource again
    ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.absent(
        ctx, name=shared_data["name"], resource_id=shared_data["resource_id"]
    )

    assert (
        f"aws.ec2.vpc_endpoint_service_configuration '{shared_data['name']}' already absent"
        in ret["comment"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
