"""Tests for validating Rds Db Proxys."""
import uuid

import pytest

PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="create")
async def test_create(
    hub, ctx, aws_iam_role, aws_secretsmanager_secret, aws_ec2_subnet, aws_ec2_subnet_2
):
    r"""
    **Test function**
    """

    # Test - create new resource
    global PARAMETER
    auth = {
        "AuthScheme": "SECRETS",
        "ClientPasswordAuthType": "POSTGRES_SCRAM_SHA_256",
        "IAMAuth": "DISABLED",
        "SecretArn": aws_secretsmanager_secret.get("resource_id"),
    }
    ret = await hub.exec.aws.rds.db_proxy.create(
        ctx,
        name=PARAMETER["name"],
        db_proxy_name=PARAMETER["name"],
        engine_family="POSTGRESQL",
        auth=[auth],
        role_arn=aws_iam_role.get("arn"),
        vpc_subnet_ids=[
            aws_ec2_subnet.get("SubnetId"),
            aws_ec2_subnet_2.get("SubnetId"),
        ],
        idle_client_timeout=5400,
        tags={"Name": "idem-test"},
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    PARAMETER["resource_id"] = resource.get("resource_id", None)
    assert PARAMETER["resource_id"]
    assert PARAMETER["name"] == PARAMETER["resource_id"]

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["name"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="get", depends=["create"])
async def test_get(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Invalid/Not-Found resource lookup
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.rds.db_proxy.get(
        ctx,
        name=PARAMETER["name"],
        resource_id="invalid-resource-id",
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    ret = await hub.exec.aws.rds.db_proxy.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="list", depends=["create"])
async def test_list(hub, ctx):
    r"""
    **Test function**
    """

    global PARAMETER
    ret = await hub.exec.aws.rds.db_proxy.list(ctx)
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    for resource in ret["ret"]:
        assert resource


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["list"])
async def test_update(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Update existing resource
    global PARAMETER

    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.rds.db_proxy.update(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        idle_client_timeout=5200,
        tags={"Name": "idem-test-1"},
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert {"Name": "idem-test-1"} == resource["tags"]
    assert 5200 == resource["idle_client_timeout"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="delete", depends=["update"])
async def test_delete(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Delete existing resource
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.rds.db_proxy.delete(
        ctx, name="", resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])
