import uuid

import pytest
import pytest_asyncio

DB_CLUSTER_NAME = f"idem-test-db-cluster-{str(uuid.uuid4())}"
PARAMETER = {
    "resource_id": DB_CLUSTER_NAME,
}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_get(hub, ctx, aws_rds_db_subnet_group):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER

    await _create_db_cluster(hub, ctx, aws_rds_db_subnet_group)

    ret = await hub.exec.aws.rds.db_cluster.get(
        ctx,
        **PARAMETER,
    )

    assert ret["ret"]["resource_id"] == PARAMETER["resource_id"]

    ret = await hub.states.aws.rds.db_cluster.absent(
        ctx,
        name=PARAMETER["resource_id"],
        resource_id=PARAMETER["resource_id"],
        skip_final_snapshot=True,
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_rds_db_subnet_group):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    await _create_db_cluster(hub, ctx, aws_rds_db_subnet_group)

    ret = await hub.exec.aws.rds.db_cluster.list(
        ctx,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], list)
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert PARAMETER["resource_id"] == resource.get("resource_id")

    ret = await hub.states.aws.rds.db_cluster.absent(
        ctx,
        name=PARAMETER["resource_id"],
        resource_id=PARAMETER["resource_id"],
        skip_final_snapshot=True,
    )


async def _create_db_cluster(hub, ctx, aws_rds_db_subnet_group):
    """
    Create test db cluster
    """
    db_cluster_name = PARAMETER["resource_id"]
    engine = "aurora-postgresql"
    master_username = "admin123"
    master_user_password = "abcd1234"
    backup_retention_period = 7
    database_name = "dbname123"
    engine_version = "12.7"
    port = 5432
    preferred_backup_window = "07:41-08:11"
    preferred_maintenance_window = "sat:09:29-sat:09:59"
    storage_encrypted = True
    engine_mode = "provisioned"
    deletion_protection = False
    copy_tags_to_snapshot = True
    db_subnet_group_name = aws_rds_db_subnet_group.get("resource_id")
    tags = {"Name": db_cluster_name}
    cloudwatch_logs_export_configuration = {"EnableLogTypes": ["postgresql"]}

    # Create DB Cluster with test flags
    ret = await hub.states.aws.rds.db_cluster.present(
        ctx,
        name=db_cluster_name,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        database_name=database_name,
        engine_version=engine_version,
        port=port,
        preferred_backup_window=preferred_backup_window,
        preferred_maintenance_window=preferred_maintenance_window,
        storage_encrypted=storage_encrypted,
        engine_mode=engine_mode,
        deletion_protection=deletion_protection,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_subnet_group_name=db_subnet_group_name,
        tags=tags,
        apply_immediately=True,
        allow_major_version_upgrades=True,
        cloudwatch_logs_export_configuration=cloudwatch_logs_export_configuration,
    )
