import pytest
from dict_tools.differ import deep_diff


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_apigateway_rest_api):
    ret = await hub.exec.aws.apigateway.rest_api.get(
        ctx,
        name=aws_apigateway_rest_api["name"],
        resource_id=aws_apigateway_rest_api["resource_id"],
    )
    assert ret["result"], ret["comment"]

    expected_ret = {
        "name": aws_apigateway_rest_api["name"],
        "resource_id": aws_apigateway_rest_api["resource_id"],
        "version": "V1",
        "binary_media_types": [],
        "api_key_source": "HEADER",
        "endpoint_configuration": {"types": ["EDGE"]},
        "tags": {},
        "disable_execute_api_endpoint": False,
        "root_resource_id": ret["ret"]["root_resource_id"],
    }
    assert len(deep_diff(expected_ret, ret["ret"])) == 0


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_none(hub, ctx):
    ret = await hub.exec.aws.apigateway.rest_api.get(
        ctx, name="aname", resource_id="invalid_id"
    )
    assert not ret["result"]
    assert "NotFoundException" in str(ret["comment"])
